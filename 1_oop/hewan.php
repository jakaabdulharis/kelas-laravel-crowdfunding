<?php
trait Hewan
{
    public $name;
    public $darah = 50;
    public $jumlah_kaki;
    public $keahlian;

    function set_name($name) {
        $this->name = $name;
    }
    public function atraksi(){
        return $this->name." sedang ".$this->keahlian;
    } 
}

trait Fight
{
    public $attackPower;
    public $defencePower;

    public function serang($penyerang,$yangdiserang){
        return $penyerang . " sedang menyerang " . $yangdiserang . "<br>";
    }

    public function diserang($yangdiserang,$penyerang,$darah_yangdiserang, $attackPower, $defencePower){
        $this->darah = $darah_yangdiserang - ($attackPower/$defencePower);
        return $yangdiserang . " sedang diserang " . $penyerang . "<br>";
    }
}


class Harimau{
    use Hewan,Fight;
    public function getInfoHewan(){
        return 'Nama Hewan = ' . $this->name . '<br>' . 'Jumlah Kaki = '. $this->jumlah_kaki . '<br>' . 'Darah = ' . $this->darah . '<br>' . 'Keahlian = ' .$this->keahlian . '<br>' . 'Attack Power = ' . $this->attackPower . '<br>' . "Defence Power = " . $this->defencePower;  
    }
}

class Elang{
    use Hewan,Fight;
    public function getInfoHewan(){
        return 'Nama Hewan = ' . $this->name . '<br>' . 'Jumlah Kaki = '. $this->jumlah_kaki . '<br>' . 'Darah = ' . $this->darah . '<br>' . 'Keahlian = ' .$this->keahlian . '<br>' . 'Attack Power = ' . $this->attackPower . '<br>' . "Defence Power = " . $this->defencePower;  
    }
}

echo "-------------------------------------------------------------------------------------------------------------------<br>";
echo "Tugas 1 - Kelas Pembuatan Website Crowdfunding Menggunakan Laravel + Vue JS (Batch 22)<br>";
echo "-------------------------------------------------------------------------------------------------------------------<br>";

echo "<b>Class Elang</b><br>";
$elang = new Elang();
$elang->set_name('Elang');
$elang->jumlah_kaki = 2;
$elang->keahlian = "terbang tinggi";
$elang->attackPower = 10;
$elang->defencePower = 5;
echo $elang->getInfoHewan();
echo "<br><b>Method atraksi()</b><br>";
echo $elang->atraksi();
echo "<br>----------------------------------<br>";

echo "<b>Class Harimau</b><br>";
$harimau = new Harimau();
$harimau->set_name('Harimau');
$harimau->jumlah_kaki = 4;
$harimau->keahlian = "lari cepat";
$harimau->attackPower = 7;
$harimau->defencePower = 8;
echo $harimau->getInfoHewan();
echo "<br><b>Method atraksi()</b><br>";
echo $harimau->atraksi();

echo "<br>----------------------------------<br>";
echo "<b>Method serang() dan diserang()</b><br>";
echo $harimau->serang($harimau->name, $elang->name, $harimau->darah,$elang->attackPower,$harimau->defencePower);
echo $elang->diserang($elang->name, $harimau->name,$elang->darah,$harimau->attackPower,$elang->defencePower);
echo "Darah Harimau sekarang = ".$harimau->darah . '<br>';
echo "Darah Elang Sekarang = " .$elang->darah.'<br>';
echo "----------------------------------<br>";
echo $elang->serang($elang->name, $harimau->name, $harimau->darah,$elang->attackPower,$harimau->defencePower);
echo $harimau->diserang($harimau->name, $elang->name,$elang->darah,$harimau->attackPower,$elang->defencePower);
echo "Darah Harimau sekarang = ".$harimau->darah . '<br>';
echo "Darah Elang Sekarang = " .$elang->darah.'<br><br>';
